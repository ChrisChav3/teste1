﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    class ProdutoDataBase
    {
        public int Salvar(ProdutoDTO dto)
        {
            string script = @"Insert into tb_produto (id_produto, nm_produto, vl_preco) Values (@id_produto, @nm_produto, @vl_preco)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", dto.NumeroProduto));
            parms.Add(new MySqlParameter("nm_produto", dto.NomeProduto));
            parms.Add(new MySqlParameter("vl_preco", dto.ValorProduto));

            Database Db = new Database();
            return Db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<ProdutoDTO> Listar()
        {
            string script = @"select * from tb_produto";


            List<MySqlParameter> parms = new List<MySqlParameter>();


            Database Db = new Database();
            MySqlDataReader reader = Db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();

            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.NumeroProduto = reader.GetInt32("id_produto");
                dto.NomeProduto = reader.GetString("nm_produto");
                dto.ValorProduto = reader.GetDecimal("vl_preco");

                lista.Add(dto);
            }

            reader.Close();

            return lista;

        }

        public List<ProdutoDTO> Consultar(string NomeProduto)
        {
            string script = @"select * from tb_produto WHERE nm_produto Like @nm_produto";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", NomeProduto + "%"));


            Database Db = new Database();
            MySqlDataReader reader = Db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();

            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.NumeroProduto = reader.GetInt32("id_produto");
                dto.NomeProduto = reader.GetString("nm_produto");
                dto.ValorProduto = reader.GetDecimal("vl_preco");

                lista.Add(dto);
            }

            reader.Close();

            return lista;


        }
    }
}
 