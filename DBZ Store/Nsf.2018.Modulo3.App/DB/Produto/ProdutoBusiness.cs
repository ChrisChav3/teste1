﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    class ProdutoBusiness
    {
        public int Salvar (ProdutoDTO Produto)
        {
            ProdutoDataBase Db = new ProdutoDataBase();
            return Db.Salvar(Produto);
            
        }

        public List <ProdutoDTO> listar ()
        {
            ProdutoDataBase Db = new ProdutoDataBase();
            return Db.Listar();

        }

        public List <ProdutoDTO> Consultar (string NomeProduto)
        {
            ProdutoDataBase Db = new ProdutoDataBase();
            return Db.Consultar(NomeProduto);
        }
    }
}
