﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    class ProdutoDTO
    {

        public string NomeProduto { get; set; }
        public decimal NumeroProduto { get; set; }
        public decimal ValorProduto { get; set; }

    }
}
