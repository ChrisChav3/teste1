﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    class PedidoDataBase
    {
        public int Salvar(PedidoDTO dto)
        {
            string script = @"Insert into tb_Pedido(nm_cliente,ds_cpf,dt_venda) Values(@nm_cliente,@ds_cpf,@dt_venda)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", dto.Cliente));
            parms.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parms.Add(new MySqlParameter("dt_venda", dto.Data));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }

        public List<PedidoConsultarView> Consultar(string cliente)

        {
            string script = @"SELECT * FROM vw_pedido_consultar WHERE nm_cliente like @nm_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", cliente + "%"));

            Database db = new Database();
            MySqlDataReader Reader = db.ExecuteSelectScript(script, parms);

            List<PedidoConsultarView> Dto = new List<PedidoConsultarView>();

            while (Reader.Read())
            {
                PedidoConsultarView Lista = new PedidoConsultarView();
                Lista.id = Reader.GetInt32("id_pedido");
                Lista.cliente = Reader.GetString("nm_cliente");
                Lista.CPF = Reader.GetString("ds_cpf");
                Lista.QTDitens = Reader.GetInt32("qtd_itens");
                Lista.Data = Reader.GetDateTime("dt_venda");
                Lista.total = Reader.GetDecimal("vl_total");

                Dto.Add(Lista);


            }

            Reader.Close();

            return Dto;

           


        }
    }
}
