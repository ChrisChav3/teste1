﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    class PedidoConsultarView
    {
        public int id { get; set; }
        public string cliente { get; set; }
        public string CPF { get; set; }
        public int QTDitens { get; set; }
        public DateTime Data { get; set; }
        public decimal total { get; set; }
    }
}
