﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    class PedidoDTO
    {
        public string Produto {get;set;}
        public int Quantidade {get;set;}
        public string Cliente {get;set;}
        public int CPF {get;set;}
        public int ID {get;set;}
        public int Data {get;set;}
        public decimal Total {get;set;}
    }
}
